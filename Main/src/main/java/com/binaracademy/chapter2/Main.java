package com.binaracademy.chapter2;

import com.binaracademy.chapter2.model.*;

public class Main {
    public static void main(String[] args) {
        Manusia kambing = new Manusia();
        kambing.setNama("Kambing");
        kambing.setUmur(23);
        System.out.println(kambing.getNama()+" berumur "+ kambing.getUmur());
    }
}
