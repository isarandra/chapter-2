package MeanMedianModus;

public class Main {
    public static void main(String[] args) {
        int[] input = {1,4,2,7,4,3,4,9,2,1,4,2,5,5,4,2,1,8,4,3};

        ListData listData = new Calculation();

        listData.setListInput(input);
        listData.printInputList();
        System.out.print("\nSorted input: ");
        listData.sort(input);
        listData.printInputList();
        listData.modus(input);
        listData.mean(input);
        listData.median(input);
    }
}
