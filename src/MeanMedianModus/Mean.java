package MeanMedianModus;

public abstract class Mean extends ListData {
    public void mean(int[] input) {
        int count=0;
        double sum=0;
        for(double number:input){
            sum+=number;
            count++;
        }
        System.out.print("Mean: "+sum/count);
        System.out.println(" ");
    }
    public abstract void median(int[] input);
    public abstract void modus(int[] input);
}
