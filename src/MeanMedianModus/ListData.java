package MeanMedianModus;

public abstract class ListData {
    private int[] input;
    private int dummy, count = 0;

    public void setListInput(int[] input) {
        this.input = input;
    }
    public abstract void mean(int[] input);
    public abstract void median(int[] input);
    public abstract void modus(int[] input);
    public void printInputList() {
        System.out.print("Input: ");
        System.out.print("[");
        for (int i=0;i<input.length;i++){
            if (i==input.length-1){
                System.out.print(input[i]);
            }
            else{
                System.out.print(input[i] + ", ");
            }
        }
        System.out.print("]");
    }
    public void sort(int[] list) {
        if (count == list.length){
            return;
        }
        for(int i=1;i<list.length;i++){
            if(list[i-1]>list[i]){
                dummy=list[i-1];
                list[i-1]=list[i];
                list[i]=dummy;
            }
        }
        count++;
        sort(list);
    }
}
