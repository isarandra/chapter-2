package MeanMedianModus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Modus extends Median{
    public void modus(int[] input) {
        HashMap<Integer,Integer> numberAppearance = new HashMap<Integer, Integer>();

        for(int number:input){
            if(numberAppearance.containsKey(number)){
                numberAppearance.computeIfPresent(number,(key, val)->val+1);
            }
            else{
                numberAppearance.computeIfAbsent(number,k->1);
            }
        }
        System.out.println(" ");
        System.out.print("Appearance count: ");
        System.out.println(numberAppearance);
        System.out.print("Modus: ");
        int modusValue = -1;
        int modusKey=0;
        int numberOfModus=0;
        ArrayList<Integer> modusKeyList=new ArrayList<>();
        for(Map.Entry<Integer, Integer> number:numberAppearance.entrySet()){
            if(number.getValue()>modusValue){
                modusValue=number.getValue();
                modusKey=number.getKey();
            }
        }
        for(Map.Entry<Integer, Integer> number:numberAppearance.entrySet()){
            if(number.getValue()==modusValue){
                modusKeyList.add(number.getKey());
                numberOfModus++;
            }
        }
        if(numberOfModus>1){
            System.out.println(modusKeyList);
        }
        else {
            System.out.println(modusKey);
        }
    }
}
