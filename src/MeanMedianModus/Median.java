package MeanMedianModus;

public abstract class Median extends Mean{
    public void median(int[] input) {
        double medianIndex;
        double median;
        if(input.length%2==0){
            median=(input[input.length/2-1]+input[input.length/2])/2.0;
            System.out.print("Median: " + median);
        }
        else{
            medianIndex=input.length/2;
            System.out.print("Median: " + input[(int)medianIndex]);
        }
    }
    public abstract void modus(int[] input);
}
