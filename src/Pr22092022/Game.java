package Pr22092022;

public abstract class Game {
    private String name;
    private double rating;
    private int numberOfDownloads;


    abstract String installationStatus();
    abstract boolean isFavorite();



    public void description(){
        System.out.println("Game is fun!");
    }

    public void description(int hoursPlayed){
        System.out.println("You've been playing games for " + hoursPlayed +" hours!");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getNumberOfDownloads() {
        return numberOfDownloads;
    }

    public void setNumberOfDownloads(int numberOfDownloads) {
        this.numberOfDownloads = numberOfDownloads;
    }
}
