package Pr22092022;

public class Main {
    public static void main(String[] args) {
        Game dota2 = new Dota();

        dota2.setName("Dota 2");
        dota2.setRating(10);
        dota2.setNumberOfDownloads(999999);

        System.out.print("Game: "+dota2.getName());
        System.out.print("\nInstallation status: "+dota2.installationStatus());
        System.out.println("\n"+dota2.getName()+" rating is "+dota2.getRating()
                +" out of 10.0 with "+dota2.getNumberOfDownloads()+" number of downloads.");

        dota2.description(10000);
        if(dota2.isFavorite()){
            dota2.description();
        }
    }
}
