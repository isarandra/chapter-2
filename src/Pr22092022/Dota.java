package Pr22092022;

public class Dota extends Game{

    @Override
    String installationStatus() {
        return "Installed.";
    }
    @Override
    boolean isFavorite() {
        return true;
    }

    @Override
    public void description() {
        System.out.println("Dota 2 is the best!");
    }

    @Override
    public void description(int hoursPlayed) {
        System.out.println("You've been playing Dota 2 for " + hoursPlayed +" hours!");
    }
}
