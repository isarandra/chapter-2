import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Modus {
    public static void main(String[] args) {
//        int[] input = {75,25,50,60,75,65,45,75,75,65,35,65,90,75,50,45,75,75,35,95,95,55,75,65,75};
//        int[] input = {6,3,9,6,6,5,9,3};
        int[] input = {1,3,3,3,5,6,6,9,9,9};
        printList(input);
        modus(input);
    }

    private static void modus(int[] listInput) {
        HashMap<Integer,Integer> numberAppearance = new HashMap<Integer, Integer>();

        for(int number:listInput){
            if(numberAppearance.containsKey(number)){
                numberAppearance.computeIfPresent(number,(key, val)->val+1);
            }
            else{
                numberAppearance.computeIfAbsent(number,k->1);
            }
        }
        System.out.println(" ");
        System.out.print("Appearance count: ");
        System.out.println(numberAppearance);
        System.out.print("Modus: ");
        int modusValue = -1;
        int modusKey=0;
        int numberOfModus=0;
        ArrayList<Integer> modusKeyList=new ArrayList<>();
        for(Map.Entry<Integer, Integer> number:numberAppearance.entrySet()){
            if(number.getValue()>modusValue){
                modusValue=number.getValue();
                modusKey=number.getKey();
            }
        }
        for(Map.Entry<Integer, Integer> number:numberAppearance.entrySet()){
            if(number.getValue()==modusValue){
                modusKeyList.add(number.getKey());
                numberOfModus++;
            }
        }
        if(numberOfModus>1){
            System.out.println(modusKeyList);
        }
        else {
            System.out.println(modusKey);
        }
    }
    private static void printList(int[] list) {
        System.out.print("Input: ");
        System.out.print("[");
        for (int i=0;i<list.length;i++){
            if (i==list.length-1){
                System.out.print(list[i]);
            }
            else{
                System.out.print(list[i] + ", ");
            }
        }
        System.out.print("]");
    }
}