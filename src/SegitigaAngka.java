import java.util.Scanner;

//public class SegitigaAngka {
//    public static void main(String[] args) {
//        System.out.print("Enter rows: ");
//        Scanner scanner = new Scanner(System.in);
//        int rows = scanner.nextInt();
//        System.out.println(" ");
//        for(int i=rows;i>=1;i--){
//            for(int j=i;j<rows;j++){
//                System.out.print(" ");
//            }
//            for(int j=1;j<=i;j++){
//                System.out.print(j);
//                if(j==i){
//                    for(int k=j-1;k>=1;k--){
//                        System.out.print(k);
//                    }
//                }
//            }
//            System.out.print("\n");
//        }
//    }
//}

// top code is the old code

public class SegitigaAngka{
    public static void main(String[] args) {
        System.out.print("Enter rows: ");
        Scanner scanner = new Scanner(System.in);
        int rows = scanner.nextInt();
        System.out.println(" ");
        for(int i=rows;i>=1;i--){
            for(int j=i;j<rows;j++){
                System.out.print(" ");
            }
            for(int k=1;k<=i;k++){
                if(k<=i){
                    System.out.print(k);
                }
                if(k==i){
                    for(int l=k-1;l>=1;l--){
                        System.out.print(l);
                    }
                }
            }
            System.out.println(" ");
        }
    }
}