public class SortingNumbersRecursive {
    static int dummy, count = 0;

    public static void main(String[] args) {
        int[] list = {64,34,25,12,22,11,90};
        sort(list);
        printList(list);
    }

    private static void printList(int[] list) {
        System.out.print("[");
        for (int i=0;i<list.length;i++){
            if (i==list.length-1){
                System.out.print(list[i]);
            }
            else{
                System.out.print(list[i] + ", ");
            }
        }
        System.out.print("]");
    }

    private static void sort(int[] list) {
        if (count == list.length){
            return;
        }
        for(int i=1;i<list.length;i++){
            if(list[i-1]>list[i]){
                dummy=list[i-1];
                list[i-1]=list[i];
                list[i]=dummy;
            }
        }
        count++;
        sort(list);
    }
}
