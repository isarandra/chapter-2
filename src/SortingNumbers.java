public class SortingNumbers {

    public static void main(String[] args) {
        int[] list = {100,20,15,30,5,75,40};
        for (int i=0;i<list.length;i++){
            shiftValue(list);
        }
        printList(list);
    }

    private static void printList(int[] list) {
        System.out.print("[");
        for (int i=0;i<list.length;i++){
            if (i==list.length-1){
                System.out.print(list[i]);
            }
            else{
                System.out.print(list[i] + ", ");
            }
        }
        System.out.print("]");
    }

    private static void shiftValue(int[] list) {
        int dummy;
        for(int i=1;i<list.length;i++){
            if(list[i-1]>list[i]){
                dummy=list[i-1];
                list[i-1]=list[i];
                list[i]=dummy;
            }
        }
    }
}
