import java.util.ArrayList;
import java.util.Scanner;

//public class BilanganPrima {
//    public static void main(String[] args) {
//        ArrayList<Integer> list = new ArrayList<>();
//        System.out.print("Masukkan max number: ");
//        Scanner scanner = new Scanner(System.in);
//        int maxNumber=scanner.nextInt();
//
//        for(int numberToCheck=2;numberToCheck<=maxNumber;numberToCheck++){
//            boolean isPrime=true;
//            for(int factor=2;factor<=numberToCheck/2;factor++){
//                if(numberToCheck%factor==0){
//                    isPrime=false;
//                    break;
//                }
//            }
//            if(isPrime){
//                list.add(numberToCheck);
//            }
//        }
//        for (int number:list){
//            System.out.print(number+" ");
//        }
//    }
//}

// below is code from scratch

public class BilanganPrima{
    public static void main(String[] args) {
        System.out.print("Masukkan angka akhir di deret: ");
        Scanner scanner = new Scanner(System.in);
        int maxNumber=scanner.nextInt();
        System.out.println(" ");

        ArrayList<Integer> list = new ArrayList<>();

        for(int numberToCheck=2;numberToCheck<=maxNumber;numberToCheck++){
            boolean isPrime=true;
            for (int pembagi=2;pembagi<numberToCheck;pembagi++){
                if(numberToCheck%pembagi==0){
                    isPrime=false;
                    break;
                }
            }
            if(isPrime){
                list.add(numberToCheck);
            }
        }
        for(int number:list){
            System.out.print(number+" ");
        }
    }
}
