import java.util.Scanner;

public class Palindrome {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        isPalindrome(takeInput());
    }

    private static void isPalindrome(String input) {
        String reverseInput ="";
        for(int i=input.length()-1;i>=0;i--){
            reverseInput+=input.charAt(i);
        }
        if(input.equalsIgnoreCase(reverseInput)){
            System.out.println("String is palindrome");
        }
        else{
            System.out.println("String is not palindrome");
        }
    }

    private static String takeInput() {
        System.out.print("Masukkan kata: ");
        String input = scanner.next();
        return input;
    }
}
