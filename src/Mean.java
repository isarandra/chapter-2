public class Mean {
    public static void main(String[] args) {
//        int[] input = {9,3,1,8,3,6};
//        int[] input={2,2,5,6,7,8};
        int[] input={11,7,11,18,9,7,6,23,7};
        printList(input);
        mean(input);
    }

    private static void mean(int[] input) {
        int count=0;
        double sum=0;
        for(double number:input){
            sum+=number;
            count++;
        }
        System.out.println(" ");
        System.out.print("Mean: "+sum/count);
    }

    private static void printList(int[] list) {
        System.out.print("Input: ");
        System.out.print("[");
        for (int i=0;i<list.length;i++){
            if (i==list.length-1){
                System.out.print(list[i]);
            }
            else{
                System.out.print(list[i] + ", ");
            }
        }
        System.out.print("]");
    }
}
