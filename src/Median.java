public class Median {
    static int dummy, count = 0;

    public static void main(String[] args) {
//        int[] input = {1,3,3,6,7,8,9};
//        int[] input = {1,2,3,4,5,6,8,9};
//        int[] input = {5,13,9,7,1,9,2,9,11};
        int[] input = {4,3,7,8,4,5,12,4,5,3,2,3};
        printList(input);
        System.out.println("\nSorting input...");
        sort(input);
        printList(input);
        median(input);
    }

    private static void median(int[] input) {
        double medianIndex;
        double median;
        if(input.length%2==0){
            median=(input[input.length/2-1]+input[input.length/2])/2.0;
            System.out.print("\nMedian: " + median);
        }
        else{
            medianIndex=input.length/2;
            System.out.print("\nMedian: " + input[(int)medianIndex]);
        }

    }

    private static void printList(int[] list) {
        System.out.print("Input: ");
        System.out.print("[");
        for (int i=0;i<list.length;i++){
            if (i==list.length-1){
                System.out.print(list[i]);
            }
            else{
                System.out.print(list[i] + ", ");
            }
        }
        System.out.print("]");
    }

    private static void sort(int[] list) {
        if (count == list.length){
            return;
        }
        for(int i=1;i<list.length;i++){
            if(list[i-1]>list[i]){
                dummy=list[i-1];
                list[i-1]=list[i];
                list[i]=dummy;
            }
        }
        count++;
        sort(list);
    }
}
