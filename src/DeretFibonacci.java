import java.util.Scanner;

//public class DeretFibonacci {
//    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        System.out.print("Jumlah suku di deret: ");
//        int jumlahSuku = scanner.nextInt();
//        System.out.println(" ");
//
//        deretFibonacci(jumlahSuku);
//    }
//
//    private static void deretFibonacci(int jumlahSuku) {
//        int a=0;
//        int b=1;
//        int sum=0;
//        if(jumlahSuku==1){
//            System.out.println(a);
//        }
//        else if(jumlahSuku==2){
//            System.out.println(a+", "+b);
//        }
//        else if(jumlahSuku>2){
//            for(int i=1;i<=jumlahSuku;i++){
//                if(i!=jumlahSuku){
//                    System.out.print(a+", ");
//                    sum=a+b;
//                    a=b;
//                    b=sum;
//                }
//                else{
//                    System.out.print(a);
//                    sum=a+b;
//                    a=b;
//                    b=sum;
//                }
//            }
//        }
//    }
//}

public class DeretFibonacci{
    public static void main(String[] args) {
        System.out.println("Berapa suku?");
        Scanner scanner = new Scanner(System.in);
        int suku = scanner.nextInt();

        int a=0;
        int b=1;
        int numberHolder;
        int count =2;

        System.out.print(a);
        System.out.print(" ");
        System.out.print(b);

        while(count!=suku){

            numberHolder=a;
            a=b;
            b=numberHolder+b;

            System.out.print(" ");
            System.out.print(b);

            count++;
        }

    }
}
